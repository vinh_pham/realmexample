package mvpmodeling.exmple.com.realmversion1;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.internal.Context;

/**
 * Created by kevin on 5/5/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private final RealmResults<? extends RealmObject> LisCar;

    public MyAdapter(RealmResults<? extends RealmObject> listCars) {
        this.LisCar = listCars;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_card_view,parent,false);


        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.InjectToData(LisCar.get(position));
    }


    @Override
    public int getItemCount() {
        return LisCar.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView Name, Year, Manufacturer, Country;
        public ViewHolder(View itemView) {
            super(itemView);
            this.Name = (TextView) itemView.findViewById(R.id.CName);
            this.Year = (TextView) itemView.findViewById(R.id.CYear);
            this.Manufacturer = (TextView) itemView.findViewById(R.id.CManu);
            this.Country = (TextView) itemView.findViewById(R.id.CCountry);
        }

        public void InjectToData(RealmObject realmObject) {
            Car car = (Car) realmObject;
            this.Name.setText(car.getCarName());
            this.Year.setText(""+car.getCarYear());
            this.Manufacturer.setText(car.getCarManufacturer());
            this.Country.setText(car.getCountry());
        }
    }
}
