package mvpmodeling.exmple.com.realmversion1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycleview;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycleview = (RecyclerView) findViewById(R.id.my_recycle_view);
        recycleview.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recycleview.setLayoutManager(mLayoutManager);


        mAdapter = new MyAdapter(GetListCar());
        recycleview.setAdapter(mAdapter);


    }

    private RealmResults<? extends RealmObject> GetListCar() {
        Realm myDatabaseCars =
                Realm.getInstance(
                        new RealmConfiguration.Builder(this)
                                .name("DatabaseCar.realm")
                                .build()
                );
        if(myDatabaseCars.isEmpty())
        {
            InjectDatabase(myDatabaseCars);
            Toast.makeText(MainActivity.this, "Injected to Database", Toast.LENGTH_SHORT).show();
        }

        RealmResults<? extends RealmObject> result = myDatabaseCars.where(Car.class).findAll();

        return result;

    }

    private void InjectDatabase(Realm myDatabaseCars) {
        myDatabaseCars.beginTransaction();
        List<Car> listcar = new ArrayList<>();
        listcar.add(new Car(10,"Aventardor",2013,"Lamborghini S.p.A","Italia"));
        listcar.add(new Car(1,"Elemento",2015,"Lamborghini S.p.A","Italia"));
        listcar.add(new Car(2,"Hurracan",2016,"Lamborghini S.p.A","Italia"));
        listcar.add(new Car(3,"BMW i8",2016,"BMW","Germany"));
        listcar.add(new Car(4,"MINI COOPER 3 DOORS",2015,"MINI","England"));
        listcar.add(new Car(5,"MINI COOPER COUNTRYMAN",2017,"MINI","England"));
        listcar.add(new Car(6,"Phantom",2016,"Rolls-royce","England"));
        listcar.add(new Car(7,"Ghost",2016,"Rolls-royce","England"));
        listcar.add(new Car(8,"Wraith",2017,"Rolls-royce","England"));
        listcar.add(new Car(9,"Dawn",2017,"Rolls-royce","England"));
        myDatabaseCars.copyToRealm(listcar);
        myDatabaseCars.commitTransaction();
    }
}
