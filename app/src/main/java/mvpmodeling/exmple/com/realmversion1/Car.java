package mvpmodeling.exmple.com.realmversion1;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by kevin on 5/5/2016.
 */
public class Car extends RealmObject {

    @PrimaryKey
    int IdCode;

    @Required
    String CarName;

    int CarYear;
    String CarManufacturer;
    String Country;

    public int getIdCode() {
        return IdCode;
    }

    public void setIdCode(int idCode) {
        IdCode = idCode;
    }

    public String getCarName() {
        return CarName;
    }

    public void setCarName(String carName) {
        CarName = carName;
    }

    public int getCarYear() {
        return CarYear;
    }

    public void setCarYear(int carYear) {
        CarYear = carYear;
    }

    public String getCarManufacturer() {
        return CarManufacturer;
    }

    public void setCarManufacturer(String carManufacturer) {
        CarManufacturer = carManufacturer;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }



    public Car() {

    }

    public Car(int idCode, String carName, int carYear, String carManufacturer, String country) {

        IdCode = idCode;
        CarName = carName;
        CarYear = carYear;
        CarManufacturer = carManufacturer;
        Country = country;
    }


}
